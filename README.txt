ABOUT

This module provides a block for each menu in the system that provides a "jump
menu". A jump menu is a select box of a menu from which users can select any
menu item to be redirected straight to that page.

REQUIREMENTS

- Drupal 5.x

INSTALLATION

- Copy the jumpmenu directory to your modules directory.
- Go to admin/build/modules and enable it.
- Go to admin/build/blocks to place jump menus into block regions as desired.

AUTHOR AND CREDIT

Larry Garfield
garfield@palantir.net
http://www.palantir.net/

This module is released under the GNU General Public License v2 or, at your option, any later version.
